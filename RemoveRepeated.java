package test;

import java.util.Arrays;

/**
 * Remove Duplicate Positive Integer. Largest No. should be smaller than the
 * array length. Other approach could be done, but sorted array is required.
 * 
 * @author Ankit Nigam
 */
public class RemoveRepeated {

	public static void main(String[] args) {
		int[] inputArr = new int[] { 1, 2, 3, 4, 5, 6, 1, 2, 3 };
		System.out.println("input: " + Arrays.toString(inputArr));
		RemoveRepeated remove = new RemoveRepeated();
		int[] outArray = remove.removeRepeat(inputArr);
		System.out.println("output: " + Arrays.toString(outArray)
				+ " PS. 0's are addded by default.");
	}

	/**
	 * To remove duplicated ints & replaced with 0 so to maintain the array length
	 * @param arr input int array
	 * @return processed array 
	 */
	public int[] removeRepeat(int[] arr) {
		int size = arr.length;
		int[] newArr = new int[size];
		int j = 0;
		for (int i = 0; i < size; i++) {
			if (arr[Math.abs(arr[i])] >= 0) {
				arr[Math.abs(arr[i])] = -arr[Math.abs(arr[i])];
				newArr[j] = Math.abs(arr[i]);
				j++;
			}
		}
		return newArr;
	}
}
